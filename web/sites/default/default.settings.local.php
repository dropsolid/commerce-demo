<?php

/**
 * @file
 * Drupal site-specific local settings file.
 * settings.local.php is excluded from git repository for security reasons.
 */

/**
 * Database settings:
 */
$databases = [];
$databases['default']['default'] = [
  'database' => 'commercedemo',
  'username' => // Add string value here. This field is intentionally kept blank in the template,
  // to encourage adding random values.
  'password' => // Add string value here. This field is intentionally kept blank in the template,
  // to encourage adding random values.
  'host' => 'localhost',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
];

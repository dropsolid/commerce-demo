# Dropsolid Commerce Demo site.

Project created using https://www.drupal.org/docs/develop/using-composer/starting-a-site-using-drupal-composer-project-templates

## QUICKSTART

* Install Git https://git-scm.com/download/win
* Clone this repository
* Install Visual Studio code https://code.visualstudio.com/Download
* Open cloned repository folder in Visual Studio
* In VS code, open terminal and run `composer install`
* Save web/sites/default/default.settings.local.php as web/sites/default/settings.local.php
* Add random username and password to settings.local.php (they will be used in the next steps)
* Install Acquia Dev Desktop https://dev.acquia.com/downloads
* Important! Install to the non-default path (Windows has [limitation](https://docs.acquia.com/dev-desktop/known-issues/#devdesktop-known-windows) on the full file name length).
C:\ADD or C:\Tools\ADD should be fine.
* Launch Acquia Dev Desktop. Choose "Create site from existing code on this computer",
Choose cloned repository folder as root.
* Let ADD install the site, open it in a browser.

## Before performing the first push

Configure your git credentials:

* `git config –global user.name "Your full name or nick"`
* `git config –global user.email "youremail@yourdomain.com"`

These credentials will appear on all your commit messages in the git history.

## Issue lifecycle with git

* Switch to the main branch and pull the latest changes made by others:
  `git checkout main && git pull`
* Run `composer install` to update the local dependencies.
* Run `drush updb` to perform database changes.
* Run `drush config-import` to perform configuration changes.
* Create a new branch locally with `git checkout -b [new-branch-name]`. Branch name should be self-descripting and short,
i.e. install-webform.
* Perform the changes in the code and/or configuration.
* If there were configuration changes, export the new configuration with `drush config-export`
* Stage files for commit with `git add [file1] [folder1] ...`
* Commit changes with `git commit -m "Descriptive message of the commit"`
* Push your changes to gitlab with `git push`
* Create a merge request at https://gitlab.com/dropsolid/commerce-demo/-/merge_requests/new
* Ask for review from someone else.

## General expectations from a merge request.

* Should have short and description title.
* Description should contain a link to the issue MR is aimed to solve.
* Description should contain the list of changes.

